import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {

	// Allow us to consume the UserContext object and its properties
	const {user, setUser} = useContext(UserContext)

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    useEffect(() => {

	    // Validation to enable submit button when all fields are populated and both passwords match
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }
	}, [email, password]);

	function authenticate(e) {

	    // Prevents page redirection via form submission
	    e.preventDefault();

	    // Process a fetch request to the corresponding backend API
	    
	    /*
			Syntax : 
			fetch ('url', {options})
			.then(res => res.json())
			.then(data => {})
	    */
	    fetch ('http://localhost:4000/user/login', {
	    	// You can remove the method if you need the get method because its the default
	    	method :'POST',
	    	headers : {
	    		'Content-Type' : 'application/json'
	    	},
	    	body: JSON.stringify({
	    		email : email,
	    		password : password
	    	})
	    })
	    .then (res => res.json())
	    .then (data => {
	    	console.log(data)

	    	// If no user information is found, the "access" property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

			if (typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire ({
					title: "Login Successfull",
					icon : "success",
					text : "Welcome to Zuitt"
				})
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon : "error",
					text : "Check your login details and try again!"
				})
			}
	    })

	    // Set the email of the authenticated user in the local Storage
	    /*
			Syntax:
				localStorage.setItem('propertyName', value)
	    */
	    // Saving to a local storage
	    localStorage.setItem('email', email);

	    setUser({
	    	email : localStorage.getItem('email')
	    })

	    

	    // Clear input fields after submission
	    setEmail('');
	    setPassword('');

	}

	const retrieveUserDetails = (token) => {
		//The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs

		fetch ('http://localhost:4000/user/details', {
			method :'POST',
			headers : {
				Authorization : `Bearer ${token}`

			}
		})
		.then (res => res.json())
		.then (data => {
			console.log (data);

			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application


			setUser({
				id : data._id,
				isAdmin : data.isAdmin
			})
		})
	}




    return (

    	(user.id !== null)?

			<Navigate to ="/product" />

			:	
    		<Form onSubmit={(e) => authenticate(e)}>
	    	<h1>LOGIN</h1>
		        <Form.Group controlId="userEmail">
		            <Form.Label>Email address</Form.Label>
		            <Form.Control 
		                type="email" 
		                placeholder="Enter email"
		                value={email}
		    			onChange={(e) => setEmail(e.target.value)}
		                required
		            />
		        </Form.Group>

		        <Form.Group controlId="password">
		            <Form.Label>Password</Form.Label>
		            <Form.Control 
		                type="password" 
		                placeholder="Password"
		                value={password}
		    			onChange={(e) => setPassword(e.target.value)}
		                required
		            />
		        </Form.Group>


	            { 
	            	(isActive) ? 
		                <Button className="my-3" variant="success" type="submit" id="submitBtn">
		                    Login
		                </Button>
		                : 
		                <Button className="my-3" variant="danger" type="submit" id="submitBtn" disabled>
		                    Login
		                </Button>
	            }

	        </Form>
        
    )
}
