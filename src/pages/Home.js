import Banner from '../components/Banner.js';
// import Highlights from '../components/Highlights.js';

const data = { 
	title: "Tech Store",
	content: "We have what you need",
	destination: "/courses",
	label: "Shop Now!"
}

export default function Home() {
	return(
		<>
			<Banner className="Banner" data={data}/>
	      	{/*<Highlights/>*/}
      	</>
		)
}