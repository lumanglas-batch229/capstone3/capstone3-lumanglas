import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {useNavigate} from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from '../UserContext.js';

export default function Register(){

	const {user} = useContext(UserContext)
	const navigate = useNavigate();

	// State Hooks -> store values of the input fields
	const [firstName, setfirstName] = useState('');
	const [lastName, setlastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [number, setNumber] = useState('');


	const [isActive, setIsActive] = useState(false);

/*	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(password1);
	console.log(password2);*/

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if((firstName !== '' && lastName !=='' && email !== '' && password1 !== '' && password2 !== '' && number !=='') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	});

	function registerUser(e) {

		e.preventDefault();
		console.log("registerUser")
		fetch('http://localhost:4000/user/checkEmailAdd', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
			})
		})
    	.then(res => res.json())
    	.then(data => {

	    	// console.log(data);

    		if(data) {

				Swal.fire({
					title: 'Registration Failed',
					icon: 'error',
					text: 'Email already exists. Please try again!'
				})
			}
			else {

				fetch('http://localhost:4000/user/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
					body: JSON.stringify({
						firstName : firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo : number
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					if (data){
					Swal.fire({
						title: 'Registration Successful',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					});
					setfirstName('');
					setlastName('');
					setEmail('');
					setPassword1('')
					setPassword2('');
					setNumber('');

					alert("Thank you for registering!");
					navigate('/login');


		    	}
		    	else{

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                          });

                    }
                })


            }
        })
    }





	 return(

        <>
            <h1 className="my-5 text-center">Register</h1>
            <Form onSubmit={e => registerUser(e)}>

            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter first name"
                    value={firstName}
                    onChange={e => setfirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter last name"
                    value={lastName}
                    onChange={e => setlastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="emailAddress">
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    onChange={e => setEmail(e.target.value)}
                    value={email}
                    required
                />
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password" 
                    placeholder="Enter Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                    type="password" 
                    placeholder="Verify Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter your phone number"
                    value={number}
                    onChange={e => setNumber(e.target.value)}
                    required
                />
            </Form.Group>

            {
                isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
            }
            </Form>
        </>
    )
}
