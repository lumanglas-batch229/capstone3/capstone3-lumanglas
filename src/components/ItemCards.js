import {Card, Button} from 'react-bootstrap';
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function ItemCard({itemProps}) {

// Checks to see if the data was successfully passed
	console.log(itemProps);
	console.log(typeof itemProps);

const {name, description, price, _id} = itemProps;

// 3 Hooks in React
// 1. useState
// 2. useEffect
// 3. useContext

	// User the useState hook for the component to be able to score state
	// States are used to keep track of information related to individual components
	// Syntax -> const [getter, setter] = useState (initialGetterValue);

const [count, setCount] = useState(0);
const [limit, setLimit] = useState(30);
console.log(useState(0));

function buy() {

	

	if(limit ==  0 ){
		alert('No more stocks');

	}else {

	setCount (count + 1);
	setLimit (limit - 1);
	console.log("Buyers" + count);
	
	}
}


return (
<Card className = "my-3">
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Card.Text>Customer:{count}</Card.Text>
	            {/*<Card.Text>Slots Available: {seats}</Card.Text>*/}
	            <Link className ="btn btn-primary" to={`/ProductView/${_id}`}>Details</Link>
	            <Button onClick ={buy} variant="primary">Buy</Button>
	            
	        </Card.Body>
	    </Card>
		);
}
