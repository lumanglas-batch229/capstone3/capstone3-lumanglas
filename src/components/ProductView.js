// import Courses from './pages/Courses.js';
import { useState, useEffect, useContext,  } from 'react'
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function ProductView(){

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrive the courseId passed via URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/product/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [])

	const purchase = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/user/product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successfully purchased!",
					icon: 'success',
					text: "You have successfully boug."
				})

				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" or "Navigate" component
				navigate("/product");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})
			}

		});
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{
								(user.id !== null) ?
								<Button onClick={() => enroll(productId)} variant="primary" block>Buy</Button>
								:
								<Link className="btn btn-danger btn-block" to="/login">Login</Link>
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
