const listData = [
	{
	    id: "ts0001",
	    name: "I Phone 12 pro",
	    description: "The iPhone 12 Pro display has rounded corners that follow a beautiful curved design, and these corners are within a standard rectangle. When measured as a standard rectangular shape, the screen is 6.06 inches diagonally (actual viewable area is less).",
	    price: 45000,
	    onOffer: true
	},
	{
	    id: "ts0002",
	    name: "Razer Blackshark V2 pro Gaming Headset",
	    description: "The definitive esports gaming headset, unleashed. Powered by Razer™",
	    price: 6000,
	    onOffer: true
	},
	{
	    id: "ts0003",
	    name: "Hyper X Cloud II Gaming Headset",
	    description: "This next-generation headset generates virtual 7.1 surround sound with distance and depth to enhance your gaming, movie or music experience.",
	    price: 8000,
	    onOffer: true
	}
]

export default listData;