const jwt = require("jsonwebtoken");

// [Section] JSON Web Tokens
/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code that can decode the encrypted information

- Imagine JWT as a gift wrapping service that secures the gift with a lock
- Only the person who knows the secret code can open the lock
- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
- This ensures that the data is secure from the sender to the receiver
*/

// Token Creation

const secret = "TECHSTOREAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

// Token Verification
/*
Analogy:
	Receive the gift and open it to verify if the sender is legitimate and gift was not tampered

*/

module.exports.verify = (req, res, next) => {

	// token is retrieved from the request header
	let token = req.headers.authorization;
	console.log(token)

	// token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		// "slice" method - removes the bearer prefix
		// Bearer 1f85a6f4d6fg4a8d67a46d54a
		token = token.slice(7, token.length);

		// validate the token using the verify method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			
			// if JWT is not valid
			if(err){
				return res.send({auth: "failed"})
			
			// if JWT is valid
			} else {
				// to proceed to the next middleware function / callback in the route
				next();
			}
		})
	
	// token does not exist
	} else {
		return res.send({auth:"failed"})
	};
}

// Token decryption
/*
Analogy
	Open the gift and get the content
*/

module.exports.decode = (token) => {

	// token received and is not undefined
	if(typeof token !== "undefined"){

		// removes the "Bearer" prefix
		token = token.slice(7, token.length);

		// verify method
		return jwt.verify(token, secret, (err, data) => {

			if(err){
				
				return null;
			
			} else {
				// decode method - used to obtain info from the JWT
				// complete: true - option allows to return additional info from the JWT token
				// payload contains info provided in the "createAccessToken" (id, email, isAdmin)
				 return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		
		return null
	}
}

