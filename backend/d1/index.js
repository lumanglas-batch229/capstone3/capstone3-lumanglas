// Installl first in console "npm i express mongoose cors"
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

// Server setup

const app = express();
const port = process.env.PORT || 4000;


// Database Connection // Mongo DB connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.iwymqfu.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));


// MiddleWares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/user", userRoutes);
app.use("/product", productRoutes);
app.use("/orders", orderRoutes);


app.listen(port, () => {
	console.log(`API is now a online on port ${port}.`)
});