const User = require("../models/Users.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
// const Product = require("../models/Product.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		
		// 10 is the value provided as the number of "salt"
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database

	return newUser.save().then((user, error) => {
		// registration failed
		if(error){
			return false;
		}else{
			// if registration is successfull
			return true;
		}
	})
}

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) => {
	console.log(data)
	return User.findById(data.userId).then(result => {
		console.log(result);

		// result.password = "";

		return result;
	});
}


module.exports.getAllUsers = () => {

	return User.find({isActive : true}).then(result => {
		return result;
	});
}

module.exports.updateUser = (reqParams, reqBody) => {

	// specify the fields of the document to be updated
	let updatedUser = {
		isAdmin : reqBody.isAdmin,
		isActive : reqBody.isActive
	};

	// findByIdUpdate(document ID, updatesToBeApplied)
	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
		// user privilage not updated
		if(error){
			return false;

		// user updated successfully	
		} else {
			return true;
		}
	})
}

