
const Order = require("../models/Orders");
const Product = require("../models/Products");
const User = require("../models/Users");






module.exports.createOrder = async (data) => {
    if(data.isAdmin == false){
        if(data.user == null) {
            return false;
        } else {
            const retrieveProduct = await Product.findById(data.order.productId);
            if(retrieveProduct.isActive){
                let newOrder = new Order({
                    userId: data.user,
                    products: {
                        productId : data.order.productId,
                        quantity : data.order.quantity
                    },
                    totalAmount: retrieveProduct.price * data.order.quantity

                })
                await newOrder.save();
                return true;
            } else {
                return false
            }
        }
    } else {
        return false
    }
}

module.exports.getOrders = (reqParams) => {
    console.log(reqParams)
    return Product.findById(reqParams.productId).then(result => {
        return result;
    });
}