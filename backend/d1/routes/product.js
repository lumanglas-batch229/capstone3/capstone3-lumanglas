const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const userController = require ("../controllers/userController");
const auth = require("../auth")

// Create a product
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));

});

router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Get single product

router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// update a product
router.put("/:productId/update", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
})

// Archive a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	
	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
	
});








// export the router object for index.js file
module.exports = router;