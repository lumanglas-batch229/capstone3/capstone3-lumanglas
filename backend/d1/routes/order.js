const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const userController = require ("../controllers/userController");
const orderController = require ("../controllers/orderController");
const auth = require("../auth")

router.post("/order", auth.verify, (req, res) => {

	const data = {
		user: auth.decode(req.headers.authorization).id,
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));

});

// Get user's orders
router.get("/getOrders/:userId", auth.verify, (req,res) => {
	const data = {
		user: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

    orderController.getOrders(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;